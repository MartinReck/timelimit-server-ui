import { readFileSync, writeFileSync } from 'fs'

const htmlFilePath = new URL('dist/index.html', import.meta.url)

writeFileSync(
  htmlFilePath,
  readFileSync(htmlFilePath)
    .toString('utf8')
    .replace(' type="module"', '')
)
